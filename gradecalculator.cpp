#include "gradecalculator.h"
#include "ui_gradecalculator.h"

GradeCalculator::GradeCalculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GradeCalculator)
{
    ui->setupUi(this);
    QObject::connect(ui->Hw1SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw1Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw2SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw2Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw3SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw3Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw4SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw4Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw5SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw5Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw6SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw6Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw7SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw7Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw8SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Hw8Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Midterm1SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Midterm1Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Midterm2SpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->Midterm2Slider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->FinalExamSpinBox,SIGNAL(valueChanged(int)), this, SLOT(update_all(int)));
    QObject::connect(ui->FinalExamSlider, SIGNAL(sliderMoved(int)), this, SLOT(update_all(int)));
}

GradeCalculator::~GradeCalculator()
{
    delete ui;
}

void GradeCalculator::update_all(int) {
    int hw1 = ui->Hw1SpinBox->value();
    int hw2 = ui->Hw2SpinBox->value();
    int hw3 = ui->Hw3SpinBox->value();
    int hw4 = ui->Hw4SpinBox->value();
    int hw5 = ui->Hw5SpinBox->value();
    int hw6 = ui->Hw6SpinBox->value();
    int hw7 = ui->Hw7SpinBox->value();
    int hw8 = ui->Hw8SpinBox->value();
    int midterm1 = ui->Midterm1SpinBox->value();
    int midterm2 = ui->Midterm2SpinBox->value();
    int finalexam = ui->FinalExamSpinBox->value();

    double schemaA = 0.25*(hw1+hw2+hw3+hw4+hw5+hw6+hw7+hw8)/8.0 + .20*midterm1 + .20*midterm2 + .35*finalexam;
    double schemaB = 0;
    if (midterm1 <= midterm2)
        schemaB = 0.25*(hw1+hw2+hw3+hw4+hw5+hw6+hw7+hw8)/8.0 + .30*midterm2 + .44*finalexam;
    else
        schemaB = 0.25*(hw1+hw2+hw3+hw4+hw5+hw6+hw7+hw8)/8.0 + .30*midterm1 + .44*finalexam;

    ui->SchemaAGrade->setText(QString::number(schemaA));
    ui->SchemaBGrade->setText(QString::number(schemaB));

    if(schemaA < schemaB)
        ui->OverallGradeScore->setText(QString::number(schemaB));
    else
        ui->OverallGradeScore->setText(QString::number(schemaA));
    return;
}
